
const express= require('express');

var app = express();

const hbs = require('hbs');

const port = process.env.PORT || 3000 ;

hbs.registerPartials(__dirname+'/views/partials');

// app.use((req,res,next)=>{

// 	res.render('maintenance.hbs');

// 	next();
// 		});

hbs.registerHelper('screamIt',(text)=>{

	return text.toUpperCase(); 
})

hbs.registerHelper('currentYear',()=>{

return new Date()+"  test";
});
app.set('view engine','hbs');

app.use(express.static(__dirname+'/public'));

// app.get('/',(req,res)=>{

// 	//res.send('<h1>Hello Express</h1>');

// 	res.send({

// 		name:'sayan',
// 		home: 'sodepur',
// 		food:['Rice','Dal','Chicken'],
// 		pincode:700110



// 	});
// });

app.get('/about',(req,res)=>{

	res.render('about.hbs',{

		pageTitle:'Our About Us'
	});

});

app.get('/',(req,res)=>{

	res.render('home.hbs',{

		pageTitle:'Our Home',

		bdy:'This is Body'
	});

});

app.get('/bad',(req,res)=>{

	res.send({

		errorMessage:'Something Went Wrong'

	});

});

	
app.listen(port,()=>{

	console.log(`Server Runs ${port}`);
});